package Interfaces;

public interface Measurable 
{
	double getMeasure();
}
