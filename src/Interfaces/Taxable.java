package Interfaces;

public interface Taxable 
{
	double getTax();
}
