package Control;

import Interfaces.Taxable;
import Model.Company;
import Model.Product;
import Model.TaxCalculator;
import Model.Data;
import Interfaces.Measurable;
import Model.Person;

public class TestCase {

	public void TestPerson_part()
	{
		Measurable[] Persons = new Measurable[5];
		Persons[0] = new Person("FrydH",190,300000);
		Persons[1] = new Person("iiamnut",150,100000);
		Persons[2] = new Person("Noey",140,150000);
		Persons[3] = new Person("Mhew",160,250000);
		Persons[4] = new Person("Dear",170,200000);
		
		for(int i=0; i<Persons.length;i++)
		{
			System.out.println(Persons[i].toString());
			
		}
		
		System.out.println("\nPerson average height : "+Data.average(Persons));
		System.out.println("************************************************");
		
	}
	
	public void TestMin_part()
	{
		Measurable[] Persons = new Measurable[6];
		Measurable[] Persons2 = new Measurable[3];
		
		Persons[0] = new Person("FrydH",190,300000);
		Persons[1] = new Person("iiamnut",150,100000);
		Persons[2] = new Person("Noey",140,150000);
		Persons[3] = new Person("Mhew",160,250000);
		Persons[4] = new Person("Dear",170,200000);
		Persons[5] = new Person("Ploum",180,280000);
		
		for(int i=0; i<Persons.length;i++)
		{
			System.out.println(Persons[i].toString());
			
		}
		
		Persons2[0] = Data.min(Persons[0],Persons[1]);
		Persons2[1] = Data.min(Persons[2],Persons[3]);
		Persons2[2] = Data.min(Persons[4],Persons[5]);
		
		System.out.println("Average height : "+Data.average(Persons2));
		System.out.println("************************************************");
		
		
		
	}
	
	public void TestTax_part()
	{
		Person[] Persons = new Person[2];
		Company[] Companys = new Company[2];
		Product[] Products = new Product[2];
		Taxable[] Taxable = new Taxable[3];
		
		Persons[0] = new Person("FrydH",190,300000);
		Persons[1] = new Person("Ploum",180,280000);
		
		for(int i=0; i<Persons.length;i++)
		{
			System.out.println(Persons[i].toString());
			
		}
		System.out.println();
		
		Companys[0] = new Company("MAC_company",2000000,400000);
		Companys[1] = new Company("Luis_company",3000000,700000);
		
		for(int i=0; i<Companys.length;i++)
		{
			System.out.println(Companys[i].toString());
			
		}
		System.out.println();
		
		Products[0] = new Product("Perfume",500);
		Products[1] = new Product("Luis_Bag",5000);
		
		for(int i=0; i<Products.length;i++)
		{
			System.out.println(Products[i].toString());
			
		}
		System.out.println();
		
		Taxable[0] = new Person("Ploum",150,300000);
		Taxable[1] = new Company("MAC_company",2000000,400000);
		Taxable[2] = new Product("Perfume",500);
		
		for(int i=0; i<Taxable.length;i++)
		{
			System.out.println(Taxable[i].toString());
			
		}
		System.out.println("\n::Result::");
		
		System.out.println("Person sum tax : "+TaxCalculator.sum(Persons));
		System.out.println("Company sum tax : "+TaxCalculator.sum(Companys));
		System.out.println("Product sum tax : "+TaxCalculator.sum(Products));
		System.out.println("All class sum tax : "+TaxCalculator.sum(Taxable));
		
		
		
	}

	public static void main(String[] args)
	{
		TestCase testing = new TestCase();
		System.out.println("Test :: Person Part");
		testing.TestPerson_part();
		System.out.println("Test :: Min Part");
		testing.TestMin_part();
		System.out.println("Test :: Tax Part");
		testing.TestTax_part();
	}

}
