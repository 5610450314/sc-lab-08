package Model;

import Interfaces.Taxable;


public class Product implements Taxable
{
	String name;
	double price;

	public Product(String name,double price)
	{
		this.name = name;
		this.price = price;
	}

	@Override
	public double getTax() 
	{
		double priceTax = price * 0.07;
		return priceTax;
	}
	
	public String toString()
	{
		return "Product Name: "+name +"\nPrice: "+price;
	}

}
