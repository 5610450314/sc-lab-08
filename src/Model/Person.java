package Model;

import Interfaces.Measurable;
import Interfaces.Taxable;

public class Person implements Measurable , Taxable
{
	String name;
	double height;
	double salary;
	
	public Person(String name,double height ,double salary)
	{
		this.name = name;
		this.height = height;
		this.salary = salary;
	}
	
	public String toString()
	{
		return "Person Name: "+name +"\nHeight: "+height+" Salary: "+salary;
	}
	
	public double getMeasure()
	{
		return this.height;
	}
	
	@Override
	public double getTax()
	{
		double aTax = 0;
		double annualYear = 0;
		double floor_salary = Math.floor(this.salary); 
		if(floor_salary <= 300000)
		{
			aTax = this.salary * 0.05;
		}
		else
		{
			aTax = 300000 * 0.05;
			annualYear = this.salary - 300000;
			aTax = aTax + (annualYear * 0.1);
		}
		return aTax;
	}
		
		
		
		
		

}
