package Model;

import Interfaces.Measurable;

public class Country implements Measurable
{
	String name;
	double people;
	
	public Country(String name,double people)
	{
		this.name = name;
		this.people = people;
	}

	@Override
	public double getMeasure()
	{
		return this.people;
	}
	
	public String toString()
	{
		return "Country: "+name +"\nPeople: "+people;
	}

}
