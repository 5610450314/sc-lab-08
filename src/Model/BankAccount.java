package Model;

import Interfaces.Measurable;

public class BankAccount implements Measurable 
{
	String name;
	double amount;

	public BankAccount(String name,double amount)
	{
		this.name = name;
		this.amount = amount;
	}

	
	@Override
	public double getMeasure()
	{
		return 0;
	}

}
