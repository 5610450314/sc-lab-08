package Model;

import Interfaces.Measurable;

public class Data 
{
	public static double average(Measurable[] obj){
		double sum = 0;
		for (Measurable m: obj){
			sum += m.getMeasure();
		}
		if(obj.length>0){
			sum = sum/obj.length;
		}
		return sum;
	}

	public static Measurable min(Measurable m1, Measurable m2)
	{
		if (m1.getMeasure() < m2.getMeasure()){
			return m1;
		}
		else{
			return m2;
		}
	}

}
