package Model;

import Interfaces.Taxable;

public class TaxCalculator 
{
	public static double sum(Taxable[] taxList)
	{
		double sum = 0;
		
		for(Taxable tb : taxList)
		{
			sum = sum + tb.getTax();
		}
		
		return sum;
		
	}
	
	
}
