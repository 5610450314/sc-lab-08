package Model;

import Interfaces.Taxable;

public class Company implements Taxable 
{
	String name;
	double earning;
	double spending;
	

	public Company(String name,double earning,double spending) 
	{
		this.name = name;
		this.earning = earning;
		this.spending = spending;
	}

	@Override
	public double getTax()
	{
		double aTax = 0;
		double diff = earning - spending;
		if(diff>=0)
		{
			aTax = diff * 0.3;
		}
		
		return aTax;
	}
	
	public String toString()
	{
		return "Company Name: "+name +"\nEarning: "+earning+" Spending: "+spending;
	}

}
